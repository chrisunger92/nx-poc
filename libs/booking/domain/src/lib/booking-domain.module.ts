import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FlightService } from './services/flight.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
})
export class BookingDomainModule {
  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: BookingDomainModule,
      providers: [FlightService],
    };
  }
}
