import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Flight } from '../domain/flight';
import { FlightService } from './flight.service';

@Injectable({ providedIn: 'root' })
export class FlightFacade {
  private flightsSubject = new BehaviorSubject<Flight[]>([]);
  public flights$ = this.flightsSubject.asObservable();

  constructor(private flightService: FlightService) {}

  search(from: string, to: string): void {
    this.flightService.find(from, to).subscribe(
      (flights) => {
        this.flightsSubject.next(flights);
      },
      (err) => {
        console.error('err', err);
      }
    );
  }
}
