import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Flight } from '../domain/flight';

@Injectable({
  providedIn: 'root',
})
export class FlightService {
  flights: Flight[] = [];
  baseUrl = `http://localhost:3333/api`;

  constructor(private http: HttpClient) {}

  find(from: string, to: string): Observable<Flight[]> {
    const url = [this.baseUrl, 'flights/query?'].join('/');
    const params = new HttpParams().set('from', from).set('to', to);
    const headers = new HttpHeaders().set('Accept', 'application/json');

    const reqObj = { params, headers };
    return this.http.get<Flight[]>(url, reqObj);
  }
}
