export interface Flight {
  readonly id: number;
  readonly flightNr: string;
  readonly from: string;
  readonly to: string;
  readonly date: string;
}
