import { Component, OnInit } from '@angular/core';
import { Flight, FlightFacade } from '@nx-poc/booking/domain';

@Component({
  selector: 'nx-poc-flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.scss'],
})
export class FlightSearchComponent implements OnInit {
  from = '';
  to = '';
  flights$ = this.flightFacade.flights$;

  constructor(private flightFacade: FlightFacade) {}

  ngOnInit(): void {}

  search(): void {
    this.flightFacade.search(this.from, this.to);
  }
}
