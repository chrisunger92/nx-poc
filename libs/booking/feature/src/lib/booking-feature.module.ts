import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlightBookingComponent } from './flight-booking.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FLIGHT_BOOKING_ROUTES } from './flight-booking.routes';
import { FlightSearchComponent } from './flight-search/flight-search.component';
// eslint-disable-next-line @nrwl/nx/enforce-module-boundaries
import { BookingDomainModule } from '@nx-poc/booking/domain';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(FLIGHT_BOOKING_ROUTES),
    FormsModule,
    BookingDomainModule.forRoot(),
  ],
  declarations: [FlightBookingComponent, FlightSearchComponent],
  exports: [FlightSearchComponent],
})
export class BookingFeatureModule {}
