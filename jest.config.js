module.exports = {
  projects: [
    '<rootDir>/apps/flight-app',
    '<rootDir>/libs/boarding/domain',
    '<rootDir>/libs/boarding/feature',
    '<rootDir>/libs/booking/api',
    '<rootDir>/libs/booking/domain',
    '<rootDir>/libs/booking/feature',
    '<rootDir>/libs/shared/ui',
    '<rootDir>/libs/shared/util-auth',
    '<rootDir>/apps/api',
  ],
};
