import { Controller, Get, Param, Query } from '@nestjs/common';

import { AppService } from './app.service';

@Controller('flights')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getFlights() {
    console.log('>>> GET all flights');
    return this.appService.getFlights();
  }

  @Get('/query?')
  getFlightsFromTo(@Query('from') from: string, @Query('to') to: string) {
    console.log('>>> GET flights from ' + from + ' to ' + to);
    return this.appService.getFlightsFromTo(from, to);
  }

  @Get(':flightNr')
  getFlight(@Param('flightNr') flightNr: string) {
    console.log('>>> GET flightNr: ', flightNr);
    return this.appService.getFlight(flightNr);
  }

  // TODO add remaining api endpoints -> see service
}
