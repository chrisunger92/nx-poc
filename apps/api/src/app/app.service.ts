import { Injectable } from '@nestjs/common';

interface Flight {
  readonly id: number;
  readonly flightNr: string;
  readonly from: string;
  readonly to: string;
  readonly date: string;
}

@Injectable()
export class AppService {
  // TODO read flights from JSON or sth
  flights: Flight[] = [
    {
      id: 1,
      flightNr: 'LH1005',
      from: 'EDDF',
      to: 'EDDP',
      date: '2021-08-01T17:55',
    },
    {
      id: 2,
      flightNr: 'LH1007',
      from: 'EDDP',
      to: 'EDDF',
      date: '2021-08-01T19:45',
    },
    {
      id: 3,
      flightNr: 'LX0086',
      from: 'EDDF',
      to: 'EGLL',
      date: '2021-08-01T17:53',
    },
  ];

  getFlights(): Flight[] {
    return this.flights;
  }

  getFlight(flightNr: string): Flight {
    return this.flights
      .filter(
        (flight) => flight.flightNr.toLowerCase() === flightNr.toLowerCase()
      )
      .pop();
  }

  getFlightsFromTo(from: string, to: string): Flight[] {
    return this.flights.filter(
      (flight) =>
        (!from || flight.from.toLowerCase() === from.toLowerCase()) &&
        (!to || flight.to.toLowerCase() === to.toLowerCase())
    );
  }
}
