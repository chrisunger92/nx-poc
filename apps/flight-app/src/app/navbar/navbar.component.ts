import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nx-poc-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  routes = [{ title: 'PlaneTixx', route: 'home', type: 'logo' }];

  constructor(public route: ActivatedRoute) {}

  ngOnInit(): void {}
}
