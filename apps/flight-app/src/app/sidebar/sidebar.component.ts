import { Component, OnInit } from '@angular/core';
import {
  faHome,
  faPlane,
  faShoppingCart,
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'nx-poc-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  faHome = faHome;
  faPlane = faPlane;
  faCart = faShoppingCart;

  constructor() {}

  ngOnInit(): void {}
}
